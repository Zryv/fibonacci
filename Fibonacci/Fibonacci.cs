﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class Fibonacci
    {
        string timeOfWork;
        Int64 numberOfFibonacci;
        public Fibonacci()
        {
            timeOfWork = "NULL";
            numberOfFibonacci = 0;
        }
        private Int64 getNumber(Int64 firstPred, Int64 secondPred, int N)
        {
            if (N == 0)
                return firstPred;
            else
                return getNumber(secondPred, firstPred + secondPred, N - 1);
        }
        private void getValue(int N)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            numberOfFibonacci = getNumber(0, 1, N);
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            timeOfWork = Convert.ToString(ts.TotalMilliseconds) + "ms";
        }
        public void run(int N)
        {
            getValue(N);            
            Console.WriteLine("Полученное число: " + numberOfFibonacci);
            Console.WriteLine("Затраченное время: " + timeOfWork);
        }
    }
}
