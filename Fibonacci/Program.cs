﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.WriteLine("Введите число:");
                int n = Convert.ToInt32(Console.ReadLine());
                Fibonacci fib = new Fibonacci();
                fib.run(n);
            }           
        }
    }
}
